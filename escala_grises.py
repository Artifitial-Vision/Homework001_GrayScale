import cv2
from matplotlib import pyplot
class EscalaGrises:

    @staticmethod
    def escala_de_grises_Lightnes():
        img = cv2.imread('me-262.jpg')
        altura, anchura, canales = img.shape
        for al in range(altura):
            for an in range(anchura):
                x = (img[al, an])
                img[al, an] = (max(img[al, an]) + min(img[al, an]))/3
                z = (img[al, an])
        # cv2.imwrite('pic_escala_de_grises_luminosidad.jpg', img)
        cv2.imshow("Lightnes", img)
        cv2.waitKey(0)
        return True

    @staticmethod
    def escala_de_grises_promedio():
        img = cv2.imread('me-262.jpg')
        altura, anchura, canales = img.shape
        for al in range(altura):
            for an in range(anchura):
                x = (img[al, an])
                img[al, an] = sum(img[al, an]) / 3
                z = (img[al, an])
        # cv2.imwrite('pic_escala_de_grises_promedio.jpg', img)
        cv2.imshow("Average", img)
        cv2.waitKey(0)

    @staticmethod
    def escala_de_grises_luminosidad():
        img = cv2.imread('me-262.jpg')
        altura, anchura, canales = img.shape
        for al in range(altura):
            for an in range(anchura):
                x = (img[al, an])
                img[al, an] = (img[al, an][0] * 0.2126) + (img[al, an][1] * 0.7152) + (img[al, an][2] * 0.0722)
                z = (img[al, an])
        cv2.imshow("Luminosity", img)
        cv2.waitKey(0)

# EscalaGrises.escala_de_grises_Lightnes()
EscalaGrises.escala_de_grises_promedio()
# EscalaGrises.escala_de_grises_luminosidad()
